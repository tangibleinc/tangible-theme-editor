=== Theme Editor ===
Requires at least: 3.4
Tested up to: 5.3
Requires PHP: 5.2.4
Stable tag: trunk
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

Theme Editor allows you to edit theme files, create folder, upload files and remove any file and folder in themes and plugins.

== Description ==

#### Theme Editor allows you to edit theme files, create folder, upload files and remove any file and folder in themes and plugins. You can easily customize you themes and plugins directly.

== Installation ==

1. Upload the plugin` folder to the directory `/wp-content/plugins/`.
2. Activate the plugin using the 'Plugins' menu in WordPress.

== Screenshots ==

== Changelog ==

= 3.0.0 =

- Forked Theme Editor to integrate editor with Loops and Logic template system
- Start removing unnecessary features
