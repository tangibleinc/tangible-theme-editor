<?php
/*
Plugin Name: Tangible: Theme Editor
Plugin URI:
Description: Create, edit, upload, download, delete theme files and folders
Author:
Version: 3.0.1
Author URI:
Text Domain: theme-editor
*/
define( 'MK_THEME_EDITOR_PATH', plugin_dir_path( __FILE__ ) . '/' );
define( 'MK_THEME_EDITOR_URL', plugins_url( '/', __FILE__ ) . '/' );
define( 'MK_THEME_EDITOR_FILE', __FILE__);
define("MK_THEME_EDITOR_DIRNAME", plugin_basename( __FILE__ ));

if(!defined('MK_WPWINDOWS')) {
	$windows = false;
	if ( strtoupper( substr( PHP_OS, 0, 3 ) ) === 'WIN' ) {
	$windows = true;
	}
  define( 'MK_WPWINDOWS', $windows );
}

add_action('init', function() {
  load_plugin_textdomain('theme-editor', false, MK_THEME_EDITOR_DIRNAME . "/languages");
});

require_once __DIR__.'/app/app.php';
// require_once __DIR__.'/includes/child-theme-editor/ms_child_theme_editor.php';

new te\pa\theme_editor_app;
